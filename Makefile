all: build
.PHONY: clean test embed build play play-wasm

clean:
	rm -f ./public/cluedorl.wasm
	rm -f ./cluedorl
	rm -f ./cluedorl.exe
test:
	go test ./...
embed:
	cd assets && embed . .png .json && cd ..
build: clean test
	go build gitlab.com/bmallred/cluedorl/cmd/cluedorl
wasm: clean test
	mkdir -p public
	GOOS=js GOARCH=wasm go build -o ./public/cluedorl.wasm gitlab.com/bmallred/cluedorl/cmd/cluedorl
	cp /usr/lib/go/misc/wasm/wasm_exec.js ./public/wasm_exec.js
play: build
	./cluedorl
play-wasm: wasm
	cd public/ && serve :8080 && cd -;
