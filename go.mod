module gitlab.com/bmallred/cluedorl

go 1.15

require (
	github.com/hajimehoshi/ebiten/v2 v2.0.6
	github.com/norendren/go-fov v1.0.1
	gitlab.com/bmallred/dungeon v0.0.3
	gitlab.com/bmallred/ecs v0.2.0
	gitlab.com/bmallred/hound v0.0.3
	gitlab.com/bmallred/toolbox v0.1.4
	golang.org/x/image v0.0.0-20200927104501-e162460cd6b5
)
