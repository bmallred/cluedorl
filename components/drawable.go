package components

import (
	"encoding/json"
	"image/color"
)

// Drawable component contains information used for renderable entities.
type Drawable struct {
	Glyph               rune       `json:"glyph"`
	ForegroundColor     color.RGBA `json:"foregroundColor"`
	BackgroundColor     color.RGBA `json:"backgroundColor"`
	MaskGlyph           rune       `json:"maskGlyph"`
	MaskForegroundColor color.RGBA `json:"maskForegroundColor"`
	MaskBackgroundColor color.RGBA `json:"maskBackgroundColor"`
}

// Type of component.
func (component *Drawable) Type() string {
	return "Drawable"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Drawable) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
