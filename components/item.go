package components

import "encoding/json"

// Item which may be picked up by an entity.
type Item struct {
	Quantity   int32 `json:"quantity"`
	Level      int32 `json:"level"`
	Cursed     bool  `json:"cursed"`
	Identified bool  `json:"identified"`

	// TODO: Include time to pick-up, drop, throw?
}

// Type of component.
func (component *Item) Type() string {
	return "Item"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Item) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
