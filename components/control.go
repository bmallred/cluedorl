package components

import "encoding/json"

// Control component providing user input control.
type Control struct{}

// Type of component.
func (component *Control) Type() string {
	return "Control"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Control) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
