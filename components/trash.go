package components

import "encoding/json"

// Trash component identifies an entity marked for garbage collection.
type Trash struct{}

// Type of component.
func (component *Trash) Type() string {
	return "Trash"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Trash) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
