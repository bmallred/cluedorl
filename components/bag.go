package components

import (
	"encoding/json"

	"gitlab.com/bmallred/ecs"
)

// Bag component contains information used for containing multiple items for an entity.
type Bag struct {
	Name     string        `json:"name"`
	Capacity int           `json:"capacity"`
	Items    []*ecs.Entity `json:"items"`
}

// Type of component.
func (component *Bag) Type() string {
	return "Bag"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Bag) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
