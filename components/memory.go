package components

import (
	"encoding/json"
	"image"
)

const (
	CardUnknown = iota
	CardDiscarded
	CardOwned
	CardKnown
)

// An entity is a playable card.
type Memory struct {
	Suspects     map[string]int `json:"suspects"`
	Weapons      map[string]int `json:"weapons"`
	Rooms        map[string]int `json:"rooms"`
	LastRoom     string         `json:"lastRoom"`
	LastWaypoint image.Point    `json:"lastWaypoint"`
	NextWaypoint image.Point    `json:"nextWaypoint"`
	Degradation  int            `json:"degradation"`
}

// Type of component.
func (component *Memory) Type() string {
	return "Memory"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Memory) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
