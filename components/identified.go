package components

import "encoding/json"

// Identified component marks something as known.
type Identified struct{}

// Type of component.
func (component *Identified) Type() string {
	return "Identified"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Identified) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
