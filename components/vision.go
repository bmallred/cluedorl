package components

import "encoding/json"

// Vision component helps drive field of vision.
type Vision struct {
	Radius int `json:"radius"`
}

// Type of component.
func (component *Vision) Type() string {
	return "Vision"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Vision) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
