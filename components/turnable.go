package components

import "encoding/json"

// Turnable component identifies if an entity may take a turn
type Turnable struct{}

// Type of component.
func (component *Turnable) Type() string {
	return "Turnable"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Turnable) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
