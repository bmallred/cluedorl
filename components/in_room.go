package components

import "encoding/json"

// InRoom component determines if an entity can make a suggestion
type InRoom struct {
	Room string `json:"room"`
}

// Type of component.
func (component *InRoom) Type() string {
	return "InRoom"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *InRoom) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
