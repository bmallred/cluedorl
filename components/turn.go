package components

import "encoding/json"

// Turn component identifies if an entity has a turn.
type Turn struct {
	Taken bool `json:"taken"`
}

// Type of component.
func (component *Turn) Type() string {
	return "Turn"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Turn) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
