package components

import "encoding/json"

// Depth component contains information in regards to the depth of the levels.
type Depth struct {
	Current  int32 `json:"current"`
	Furthest int32 `json:"furthest"`
}

// Type of component.
func (component *Depth) Type() string {
	return "Depth"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Depth) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
