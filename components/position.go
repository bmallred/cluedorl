package components

import (
	"encoding/json"
	"image"
)

// Position holds the 2D coordinates.
type Position struct {
	X int32 `json:"x"`
	Y int32 `json:"y"`
}

// Type of component.
func (component *Position) Type() string {
	return "Position"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Position) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}

// Point returns the position's image coordinate.
func (component *Position) Point() image.Point {
	return image.Point{X: int(component.X), Y: int(component.Y)}
}
