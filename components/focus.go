package components

import "encoding/json"

// Focus component used in the camera system to determine what entity has current focus.
type Focus struct{}

// Type of component.
func (component *Focus) Type() string {
	return "Focus"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Focus) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
