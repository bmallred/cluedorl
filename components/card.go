package components

import "encoding/json"

// An entity is a playable card.
type Card struct {
	Category string `json:"category"`
}

// Type of component.
func (component *Card) Type() string {
	return "Card"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Card) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
