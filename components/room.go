package components

import (
	"encoding/json"
	"image"
)

// Room component describes a room physically with a card.
type Room struct {
	Rectangle image.Rectangle `json:"rectangle"`
}

// Type of component.
func (component *Room) Type() string {
	return "Room"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Room) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
