package components

import "encoding/json"

// Motion component providing an entity movement.
type Motion struct {
	Velocity  float32 `json:"velocity"`
	VelocityX float32 `json:"velocityX"`
	VelocityY float32 `json:"velocityY"`
	Rest      bool    `json:"rest"`
}

// Type of component.
func (component *Motion) Type() string {
	return "Motion"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Motion) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
