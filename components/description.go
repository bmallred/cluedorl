package components

import "encoding/json"

// Description component with information used to identify something.
type Description struct {
	Name     string `json:"name"`
	NameMask string `json:"mask"`
}

// Type of component.
func (component *Description) Type() string {
	return "Description"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Description) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
