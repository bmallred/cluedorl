package components

import "encoding/json"

// Solution component marks a card as the final solution.
type Solution struct{}

// Type of component.
func (component *Solution) Type() string {
	return "Solution"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Solution) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
