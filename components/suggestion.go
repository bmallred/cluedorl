package components

import "encoding/json"

// Suggestion component stores information about a suggestion
type Suggestion struct {
	Suspect string `json:"suspect"`
	Weapon  string `json:"weapon"`
	Room    string `json:"room"`
	Final   bool   `json:"final"`
}

// Type of component.
func (component *Suggestion) Type() string {
	return "Suggestion"
}

// FromJson unmarshals a byte array of JSON in to the component.
func (component *Suggestion) FromJson(buffer []byte) error {
	if err := json.Unmarshal(buffer, component); err != nil {
		return err
	}
	return nil
}
