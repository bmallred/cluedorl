package cluedorl

import (
	"bytes"
	"log"
	"math/rand"

	"github.com/norendren/go-fov/fov"
)

const (
	Normal int32 = iota
	Journal
	Suggest
	Accuse
	Lose
	Win
)

// GameOptions represents common information about the game to be
// used by core systems and components.
type GameOptions struct {
	Fullscreen bool
	Width      int32
	Height     int32
	Title      string
	Seed       int64
	Stop       bool
	Floors     []*Floor
	Fov        *fov.View
	Depth      int32
	Random     *rand.Rand
	Buffer     *bytes.Buffer
	Log        *log.Logger
	Mode       int32
}

func (options *GameOptions) CurrentLevel() *Floor {
	return options.Floors[options.Depth]
}
