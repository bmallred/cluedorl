package main

import (
	"bytes"
	"errors"
	"image"
	_ "image/png"
	"log"
	"math/rand"
	"time"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/assets"
	"gitlab.com/bmallred/cluedorl/gen"
	"gitlab.com/bmallred/cluedorl/systems"
	"gitlab.com/bmallred/ecs"
)

var (
	regularTermination = errors.New("regular termination")
)

type Cluedo struct {
	options      *cluedorl.GameOptions
	textures     map[string]*ebiten.Image
	entities     []*ecs.Entity
	systems      []ecs.System
	last         time.Time
	screen       *ebiten.Image
	layoutHeight int
	layoutWidth  int
}

// Add an entity to the game.
func (game *Cluedo) Add(entity *ecs.Entity) {
	game.entities = append(game.entities, entity)
}

// Remove an entity to the game.
func (game *Cluedo) Remove(entity *ecs.Entity) {
	target := -1
	for index, e := range game.entities {
		if e.ID() == entity.ID() {
			target = index
		}
	}

	if target > -1 {
		// Without memory leak
		copy(game.entities[target:], game.entities[target+1:])
		game.entities[len(game.entities)-1] = nil
		game.entities = game.entities[:len(game.entities)-1]
	}
}

// Preload game assets.
func (game *Cluedo) Preload() {
	game.textures = map[string]*ebiten.Image{}
	imagefiles := []string{"assets/16x16-RogueYun-AgmEdit.png"}
	for _, name := range imagefiles {
		// Pull assets from memory
		png, err := assets.Read(name, true)
		if err != nil {
			log.Println("failed to read image bytes", err)
			continue
		}

		// Decode the bytes
		img, _, err := image.Decode(bytes.NewReader(png))
		if err != nil {
			log.Println("failed to load image", err)
			continue
		}

		texture := ebiten.NewImageFromImage(img)
		if err != nil {
			log.Println("failed to load texture", err)
			continue
		}

		game.textures[name] = texture
	}
}

// Setup the game environment.
func (game *Cluedo) Setup() {
	options := game.options
	options.Log.Printf("You are playing with seed: %v", options.Seed)
	options.Random = rand.New(rand.NewSource(options.Seed))
	options.Floors = gen.ConstructHotel(options.Random, 50, 50, 1)
	options.Fov = gen.ConstructFov()

	players := gen.SpawnEntities(options.Random, "assets/players.json", 1, 1, options.Floors, options.Depth, game)
	characters := gen.SpawnEntities(options.Random, "assets/characters.json", 3, 6, options.Floors, options.Depth, game)

	suspects := gen.SpawnCardsByCategory(options.Random, "assets/cards.json", "Suspect", options.Floors, -1, options.Depth, game)
	weapons := gen.SpawnCardsByCategory(options.Random, "assets/cards.json", "Weapon", options.Floors, -1, options.Depth, game)
	locations := gen.SpawnCardsByCategory(options.Random, "assets/cards.json", "Room", options.Floors, len(game.options.CurrentLevel().Rooms), options.Depth, game)
	cards := []*ecs.Entity{}
	cards = append(cards, suspects...)
	cards = append(cards, weapons...)
	cards = append(cards, locations...)
	gen.DealCards(options.Random, players, characters, cards)
	gen.WipeMemories(players, suspects, weapons, locations)
	gen.WipeMemories(characters, suspects, weapons, locations)
	gen.AssignRooms(options.Floors[0], locations)

	// Declare systems
	game.systems = []ecs.System{
		&systems.InRoomSystem{Options: game.options},
		&systems.SuggestionSystem{Options: game.options},
		&systems.AiSuggestionSystem{Options: game.options},
		&systems.AiRoomSystem{Options: game.options},
		&systems.MovementSystem{Options: game.options},
		&systems.CollisionSystem{Options: game.options},
		&systems.FovSystem{Options: game.options},
		&systems.GarbageSystem{EntityManager: game, Options: game.options},
		&systems.TurnSystem{Options: game.options},
		&RenderSystem{game: game, Options: options},
		&ControlSystem{Options: options},
	}
}

func (game *Cluedo) Update() error {
	// Update the timer to calculate the next delta.
	game.last = time.Now()

	ebiten.SetFullscreen(game.options.Fullscreen)

	// Loop through all systems
	for _, system := range game.systems {
		system.Update(time.Since(game.last), game.entities)
	}

	if game.options.Stop {
		return regularTermination
	}

	return nil
}

func (game *Cluedo) Draw(screen *ebiten.Image) {
	if game.screen == nil {
		game.screen = ebiten.NewImageFromImage(screen)
	}

	scale := ebiten.DeviceScaleFactor()

	w, h := game.screen.Size()
	op := &ebiten.DrawImageOptions{}
	op.GeoM.Translate(-float64(w)/2, -float64(h)/2)
	op.GeoM.Scale(scale, scale)

	sw, sh := screen.Size()
	op.GeoM.Translate(float64(sw)/2, float64(sh)/2)
	op.Filter = ebiten.FilterLinear
	screen.DrawImage(game.screen, op)
}

func (g *Cluedo) Layout(outsideWidth, outsideHeight int) (int, int) {
	s := ebiten.DeviceScaleFactor()
	return int(float64(1280) * s), int(float64(800) * s)
}

// Run the game logic.
func (game *Cluedo) Run() {
	// Preload assets once game context has been established.
	game.Preload()

	ebiten.SetFullscreen(game.options.Fullscreen)
	ebiten.SetWindowTitle(game.options.Title)
	ebiten.SetWindowSize(1280, 800)
	ebiten.SetWindowResizable(true)

	if err := ebiten.RunGame(game); err != nil && err != regularTermination {
		log.Fatal(err)
	}
}

// Unload game assets and perform any final clean-up routines.
func (game *Cluedo) Unload() {
	for _, texture := range game.textures {
		texture.Dispose()
	}
}
