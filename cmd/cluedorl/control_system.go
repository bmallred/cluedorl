package main

import (
	"log"
	"time"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	inpututil "github.com/hajimehoshi/ebiten/v2/inpututil"
	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
)

// ControlSystem provides handling of player inputs.
type ControlSystem struct {
	Options *cluedorl.GameOptions
}

// Update entities based on player inputs.
func (system *ControlSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	for _, entity := range ecs.FilterComponent(entities, &components.Control{}) {
		keyboard(system, entity)
		gamepad(system, entity)
	}
}

func repeatingKeyPressed(key ebiten.Key) bool {
	const (
		delay    = 30
		interval = 3
	)

	d := inpututil.KeyPressDuration(key)
	if d == 1 {
		return true
	}

	if d >= delay && (d-delay)%interval == 0 {
		return true
	}

	return false
}

func keyboard(system *ControlSystem, entity *ecs.Entity) {
	motion := entity.GetComponent(&components.Motion{})
	inRoom := entity.GetComponent(&components.InRoom{})

	if repeatingKeyPressed(ebiten.KeyF) {
		system.Options.Fullscreen = !system.Options.Fullscreen
	}

	if repeatingKeyPressed(ebiten.KeyTab) || repeatingKeyPressed(ebiten.KeyI) {
		switch system.Options.Mode {
		case cluedorl.Journal:
			fallthrough
		case cluedorl.Suggest:
			fallthrough
		case cluedorl.Accuse:
			system.Options.Mode = cluedorl.Normal
		case cluedorl.Normal:
			system.Options.Mode = cluedorl.Journal
		}
	}

	if repeatingKeyPressed(ebiten.KeyLeft) || repeatingKeyPressed(ebiten.KeyA) {
		switch system.Options.Mode {
		case cluedorl.Normal:
			if motion != nil {
				m := motion.(*components.Motion)
				m.VelocityX = -m.Velocity
			}
		}
	}

	if repeatingKeyPressed(ebiten.KeyRight) || repeatingKeyPressed(ebiten.KeyD) {
		switch system.Options.Mode {
		case cluedorl.Normal:
			if motion != nil {
				m := motion.(*components.Motion)
				m.VelocityX = m.Velocity
			}
		}
	}

	if repeatingKeyPressed(ebiten.KeyUp) || repeatingKeyPressed(ebiten.KeyW) {
		switch system.Options.Mode {
		case cluedorl.Accuse:
			fallthrough
		case cluedorl.Suggest:
			selectedIndex -= 1
		case cluedorl.Normal:
			if motion != nil {
				m := motion.(*components.Motion)
				m.VelocityY = -m.Velocity
			}
		}
	}

	if repeatingKeyPressed(ebiten.KeyDown) || repeatingKeyPressed(ebiten.KeyS) {
		switch system.Options.Mode {
		case cluedorl.Accuse:
			fallthrough
		case cluedorl.Suggest:
			selectedIndex += 1
		case cluedorl.Normal:
			if motion != nil {
				m := motion.(*components.Motion)
				m.VelocityY = m.Velocity
			}
		}
	}

	if repeatingKeyPressed(ebiten.KeyEscape) || repeatingKeyPressed(ebiten.KeyQ) {
		switch system.Options.Mode {
		case cluedorl.Journal:
			fallthrough
		case cluedorl.Suggest:
			fallthrough
		case cluedorl.Accuse:
			system.Options.Mode = cluedorl.Normal
		case cluedorl.Win:
			fallthrough
		case cluedorl.Lose:
			fallthrough
		case cluedorl.Normal:
			system.Options.Stop = true
		}
	}

	if repeatingKeyPressed(ebiten.KeyX) {
		switch system.Options.Mode {
		case cluedorl.Normal:
			selectedIndex = 0
			system.Options.Mode = cluedorl.Accuse
		}
	}

	if repeatingKeyPressed(ebiten.KeySpace) {
		switch system.Options.Mode {
		case cluedorl.Suggest:
			if inRoom != nil {
				// Toggle the selected item
				selectedAction = ActionToggle
			}
		case cluedorl.Accuse:
			// Toggle the selected item
			selectedAction = ActionToggle
		case cluedorl.Normal:
			if inRoom != nil {
				selectedIndex = 0
				system.Options.Mode = cluedorl.Suggest
			}
		}
	}

	if repeatingKeyPressed(ebiten.KeyEnter) {
		switch system.Options.Mode {
		case cluedorl.Suggest:
			if inRoom != nil && selectedSuspect != "" && selectedWeapon != "" && selectedRoom != "" {
				entity.AddComponent(&components.Suggestion{
					Suspect: selectedSuspect,
					Weapon:  selectedWeapon,
					Room:    selectedRoom,
					Final:   system.Options.Mode == cluedorl.Accuse,
				})
				system.Options.Mode = cluedorl.Normal
			}
		case cluedorl.Accuse:
			// Finalize the suggestion/accusation
			if selectedSuspect != "" && selectedWeapon != "" && selectedRoom != "" {
				entity.AddComponent(&components.Suggestion{
					Suspect: selectedSuspect,
					Weapon:  selectedWeapon,
					Room:    selectedRoom,
					Final:   system.Options.Mode == cluedorl.Accuse,
				})
				system.Options.Mode = cluedorl.Normal
			}
		}
	}
}

func repeatingGamepadPressed(id ebiten.GamepadID, button ebiten.GamepadButton) bool {
	const (
		delay    = 30
		interval = 3
	)

	d := inpututil.GamepadButtonPressDuration(id, button)
	if d == 1 {
		return true
	}

	if d >= delay && (d-delay)%interval == 0 {
		return true
	}

	return false
}

func gamepad(system *ControlSystem, entity *ecs.Entity) {
	motion := entity.GetComponent(&components.Motion{})
	// inRoom := entity.GetComponent(&components.InRoom{})

	ids := ebiten.GamepadIDs()
	for _, id := range ids {
		if repeatingGamepadPressed(id, ebiten.GamepadButton7) {
			switch system.Options.Mode {
			case cluedorl.Journal:
				fallthrough
			case cluedorl.Suggest:
				fallthrough
			case cluedorl.Accuse:
				selectedIndex = 0
				system.Options.Mode = cluedorl.Normal
			case cluedorl.Normal:
				system.Options.Mode = cluedorl.Journal
			}
		}

		if repeatingGamepadPressed(id, ebiten.GamepadButton14) {
			switch system.Options.Mode {
			case cluedorl.Normal:
				if motion != nil {
					m := motion.(*components.Motion)
					m.VelocityX = -m.Velocity
				}
			}
		}

		if repeatingGamepadPressed(id, ebiten.GamepadButton12) {
			switch system.Options.Mode {
			case cluedorl.Normal:
				if motion != nil {
					m := motion.(*components.Motion)
					m.VelocityX = m.Velocity
				}
			}
		}

		if repeatingGamepadPressed(id, ebiten.GamepadButton13) {
			switch system.Options.Mode {
			case cluedorl.Journal:
				selectedIndex += 1
			case cluedorl.Normal:
				if motion != nil {
					m := motion.(*components.Motion)
					m.VelocityY = m.Velocity
				}
			}
		}

		if repeatingGamepadPressed(id, ebiten.GamepadButton11) {
			switch system.Options.Mode {
			case cluedorl.Journal:
				selectedIndex -= 1
			case cluedorl.Normal:
				if motion != nil {
					m := motion.(*components.Motion)
					m.VelocityY = -m.Velocity
				}
			}
		}

		// [buttons] 0: A, 1: B, 2: X, 3: Y
		if ebiten.IsGamepadButtonPressed(id, 0) || ebiten.IsGamepadButtonPressed(id, 1) || ebiten.IsGamepadButtonPressed(id, 2) || ebiten.IsGamepadButtonPressed(id, 3) {
			log.Println("button pressed")
			// velocityY := motion.DegradeY * motion.FactorY
			// asset := entity.GetComponent(&Asset{}).(*Asset)
			// spatial := entity.GetComponent(&Spatial{}).(*Spatial)
			// floor := float32(system.Options.Height) - (asset.Height / 2)
			// if spatial.Y == floor {
			// 	motion.VelocityY = -velocityY
			// }
		}
	}
}
