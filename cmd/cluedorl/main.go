package main

import (
	"bytes"
	"flag"
	"fmt"
	"hash/fnv"
	"io"
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/bmallred/cluedorl"
)

var (
	fullscreen bool
	seed       string
)

func init() {
	flag.BoolVar(&fullscreen, "fullscreen", false, "Start the game in fullscreen mode")
	flag.StringVar(&seed, "seed", "", "Seed for randomization")
}

func main() {
	flag.Parse()

	game := &Cluedo{}
	defer game.Unload()

	game.options = &cluedorl.GameOptions{
		Fullscreen: fullscreen,
		Width:      80 * 16,
		Height:     50 * 16,
		Title:      "Cluedo Roguelike",
		Stop:       false,
		Buffer:     bytes.NewBuffer([]byte{}),
		Seed:       plant(),
	}

	multi := io.MultiWriter(os.Stdout, game.options.Buffer)
	game.options.Log = log.New(multi, "> ", log.Ltime)

	game.Setup()
	game.Run()
	os.Exit(0)
}

func plant() int64 {
	if seed == "" {
		nano := time.Now().UTC().UnixNano()
		rnano, _ := strconv.ParseInt(Reverse(fmt.Sprintf("%d", nano)), 10, 64)
		return rnano
	}

	h := fnv.New64a()
	h.Write([]byte(seed))
	return int64(h.Sum64())
}

func Reverse(s string) string {
	n := 0
	rune := make([]rune, len(s))
	for _, r := range s {
		rune[n] = r
		n++
	}

	rune = rune[0:n]

	// Reverse
	for i := 0; i < n/2; i++ {
		rune[i], rune[n-1-i] = rune[n-1-i], rune[i]
	}

	// Convert back to utf-8
	return string(rune)
}
