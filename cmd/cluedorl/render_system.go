package main

import (
	"bufio"
	"bytes"
	"fmt"
	"image"
	"image/color"
	"io"
	"log"
	"regexp"
	"sort"
	"strings"
	"time"

	ebiten "github.com/hajimehoshi/ebiten/v2"
	ebitenutil "github.com/hajimehoshi/ebiten/v2/ebitenutil"
	text "github.com/hajimehoshi/ebiten/v2/text"
	"github.com/norendren/go-fov/fov"
	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/dungeon"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/toolbox"
	"golang.org/x/image/font"
	"golang.org/x/image/font/gofont/gomono"
	"golang.org/x/image/font/opentype"
)

const (
	ActionNone int32 = iota
	ActionToggle
)

const (
	width    = 16
	height   = 16
	fontSize = 12
	tileSize = 16
)

var (
	arcadeFont      font.Face
	fontColor       color.Color
	selectedIndex         = 0
	selectedAction  int32 = ActionNone
	selectedSuspect       = ""
	selectedWeapon        = ""
	selectedRoom          = ""
)

func init() {
	fontColor = color.RGBA{0xff, 0xff, 0xff, 0xff}

	tt, err := opentype.Parse(gomono.TTF)
	if err != nil {
		log.Fatal(err)
	}

	const dpi = 64
	arcadeFont, err = opentype.NewFace(tt, &opentype.FaceOptions{
		Size:    fontSize,
		DPI:     dpi,
		Hinting: font.HintingFull,
	})
	if err != nil {
		log.Fatal(err)
	}
}

// RenderSystem paints all drawable components to the screen.
type RenderSystem struct {
	game    *Cluedo
	Options *cluedorl.GameOptions
}

// Update the game by rendering entities with the spatial
func (system *RenderSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	if system.game.screen == nil {
		return
	}

	// log.Printf("[system] render")
	screen := system.game.screen
	sx, sy := screen.Size()
	// log.Printf("screen size: %d x %d", sx, sy)

	// Draw the map (with camera attached)
	mx, my, mw, mh := float64(sx)*0.25, float64(sy)*0.25, float64(sx)*0.5, float64(sy)*0.5
	system.drawMap(entities, int(mx), int(my), int(mw), int(mh))

	// Draw the card deck (your hand)
	dx, dy, dw, dh := 0, float64(sy)-(float64(sy)*0.25), float64(sx), float64(sy)*0.25
	system.drawDeck(entities, int(dx), int(dy), int(dw), int(dh))

	switch system.Options.Mode {
	case cluedorl.Journal:
		// Draw the detective journal
		jx, jy, jw, jh := float64(sx)-(float64(sx)*0.30), 0, float64(sx)*0.5, float64(sy)
		system.drawJournal(entities, int(jx), int(jy), int(jw), int(jh))
	case cluedorl.Suggest:
		// Draw the picker
		px, py, pw, ph := float64(sx)*0.30, float64(sy)*0.10, float64(sx)-((float64(sx)*0.30)*2), float64(sy)-((float64(sy)*0.10)*2)
		system.drawPicker(entities, int(px), int(py), int(pw), int(ph), false)
	case cluedorl.Accuse:
		// Draw the picker
		px, py, pw, ph := float64(sx)*0.30, float64(sy)*0.10, float64(sx)-((float64(sx)*0.30)*2), float64(sy)-((float64(sy)*0.10)*2)
		system.drawPicker(entities, int(px), int(py), int(pw), int(ph), true)
	case cluedorl.Lose:
		system.youLose(screen, entities)
	case cluedorl.Win:
		system.youWin(screen, entities)
	default:
		// Draw the console
		cx, cy, cw, ch := 0, 0, float64(sx), float64(sy)*0.20
		system.drawConsole(system.Options.Buffer, int(cx), int(cy), int(cw), int(ch))
	}
}

func solution(entities []*ecs.Entity) string {
	suspect := ""
	weapon := ""
	room := ""

	for _, item := range ecs.FilterComponent(entities, &components.Solution{}, &components.Card{}, &components.Description{}) {
		card := item.GetComponent(&components.Card{}).(*components.Card)
		desc := item.GetComponent(&components.Description{}).(*components.Description)

		switch card.Category {
		case "Suspect":
			suspect = desc.Name
		case "Weapon":
			weapon = desc.Name
		case "Room":
			room = desc.Name
		}
	}

	return fmt.Sprintf("It was %s in the %s with the %s", suspect, room, weapon)
}

func (system *RenderSystem) youLose(screen *ebiten.Image, entities []*ecs.Entity) {
	sx, sy := screen.Size()
	borderwidth := float64(5)
	x1, x2, y1, y2 := float64(sx)*0.25, float64(sx)*0.75, float64(sy)*0.40, float64(sy)*0.60
	ebitenutil.DrawRect(screen, x1, y1, x2, y2, color.RGBA{0x00, 0x00, 0x00, 0x80})
	ebitenutil.DrawRect(screen, x1+borderwidth, y1+borderwidth, x2-borderwidth, y2-borderwidth, color.RGBA{0x00, 0x00, 0x00, 0xc0})

	msg := "You have lost...\n\n" + solution(entities)
	drawTextWithShadowCenter(screen, msg, int(x1), int(y1)+fontSize, 1, fontColor, int(x2-x1))
}

func (system *RenderSystem) youWin(screen *ebiten.Image, entities []*ecs.Entity) {
	sx, sy := screen.Size()
	borderwidth := float64(5)
	x1, x2, y1, y2 := float64(sx)*0.25, float64(sx)*0.75, float64(sy)*0.40, float64(sy)*0.60
	ebitenutil.DrawRect(screen, x1, y1, x2, y2, color.RGBA{0x00, 0x00, 0x00, 0x80})
	ebitenutil.DrawRect(screen, x1+borderwidth, y1+borderwidth, x2-borderwidth, y2-borderwidth, color.RGBA{0x00, 0x00, 0x00, 0xc0})

	msg := "You have caught the killer!"
	drawTextWithShadowCenter(screen, msg, int(x1), int(y1)+fontSize, 1, fontColor, int(x2-x1))
}

func coord(index, width, size int) (int, int) {
	c := width / size
	x := (index % c) * size
	y := (index / c) * size
	return x, y
}

func (system *RenderSystem) drawMap(entities []*ecs.Entity, x, y, width, height int) {
	screen := system.game.screen
	screen.Fill(color.RGBA{0x00, 0x00, 0x00, 0x00})

	level := system.Options.CurrentLevel()
	dungeon := ebiten.NewImage(level.Map.Width*tileSize, level.Map.Height*tileSize)

	// All of our graphics come from the same spritesheet
	fovMap := system.Options.Fov
	if tileset, ok := system.game.textures["assets/16x16-RogueYun-AgmEdit.png"]; ok {
		drawHotel(dungeon, tileset, level, fovMap)
		drawEntities(dungeon, tileset, fovMap, entities, level.Depth)
	}
	camera := drawCamera(screen, dungeon, entities, width, height)
	drawImageAt(screen, camera, x, y, width, height)
}

func drawHotel(screen *ebiten.Image, tileset *ebiten.Image, level *cluedorl.Floor, fovMap *fov.View) {
	wx, wy := coord(35, width*width, width)
	wr := image.Rect(wx, wy, wx+height, wy+width)
	wall := tileset.SubImage(wr).(*ebiten.Image)
	wallColor := color.RGBA{169, 157, 97, 1}

	gx, gy := coord(46, width*width, width)
	gr := image.Rect(gx, gy, gx+height, gy+width)
	ground := tileset.SubImage(gr).(*ebiten.Image)
	groundColor := color.RGBA{105, 105, 105, 1}

	cx, cy := coord(34, width*width, width)
	cr := image.Rect(cx, cy, cx+height, cy+width)
	carpet := tileset.SubImage(cr).(*ebiten.Image)
	carpetColor := color.RGBA{150, 92, 33, 1}

	for _, tileData := range level.Map.GetAllTiles() {
		var texture *ebiten.Image
		x := float64(tileData.X * 16)
		y := float64(tileData.Y * 16)
		options := &ebiten.DrawImageOptions{}

		visible := fovMap.IsVisible(tileData.X, tileData.Y)
		if !visible {
			continue
		} else if tileData.Tile.Type == dungeon.TileEmpty {
			isCarpet := false
			for _, rect := range level.Rooms {
				if tileData.X <= rect.Max.X && tileData.X >= rect.Min.X && tileData.Y <= rect.Max.Y && tileData.Y >= rect.Min.Y {
					isCarpet = true
					break
				}
			}

			if isCarpet {
				texture = carpet
				options.ColorM.Scale(float64(carpetColor.R)/255, float64(carpetColor.G)/255, float64(carpetColor.B)/255, float64(carpetColor.A))
			} else {
				texture = ground
				options.ColorM.Scale(float64(groundColor.R)/255, float64(groundColor.G)/255, float64(groundColor.B)/255, float64(groundColor.A))
			}
		} else {
			texture = wall
			options.ColorM.Scale(float64(wallColor.R)/255, float64(wallColor.G)/255, float64(wallColor.B)/255, float64(wallColor.A))
		}

		options.GeoM.Translate(x, y)
		screen.DrawImage(texture, options)
	}
}

func drawEntities(screen *ebiten.Image, tileset *ebiten.Image, fovMap *fov.View, entities []*ecs.Entity, currentDepth int32) {
	for _, entity := range ecs.FilterComponent(entities, &components.Drawable{}, &components.Position{}, &components.Depth{}) {
		if entity.GetComponent(&components.Control{}) == nil {
			drawEntity(screen, tileset, fovMap, entity, currentDepth)
		}
	}

	for _, entity := range ecs.FilterComponent(entities, &components.Control{}, &components.Drawable{}, &components.Position{}, &components.Depth{}) {
		drawEntity(screen, tileset, fovMap, entity, currentDepth)
	}
}

func drawEntity(screen *ebiten.Image, tileset *ebiten.Image, fovMap *fov.View, entity *ecs.Entity, currentDepth int32) {
	depth := entity.GetComponent(&components.Depth{}).(*components.Depth)
	if depth.Current != currentDepth {
		return
	}

	position := entity.GetComponent(&components.Position{}).(*components.Position)
	x := float64(position.X * 16)
	y := float64(position.Y * 16)

	if !fovMap.IsVisible(position.Point().X, position.Point().Y) {
		return
	}

	drawable := entity.GetComponent(&components.Drawable{}).(*components.Drawable)
	fg := drawable.ForegroundColor
	gx, gy := coord(int(drawable.Glyph), width*width, width)
	rect := image.Rect(gx, gy, gx+height, gy+width)

	options := &ebiten.DrawImageOptions{}
	options.GeoM.Translate(x, y)
	options.ColorM.Scale(float64(fg.R)/255, float64(fg.G)/255, float64(fg.B)/255, float64(fg.A))

	subimage := tileset.SubImage(rect).(*ebiten.Image)
	screen.DrawImage(subimage, options)
}

func drawCamera(screen *ebiten.Image, dungeon *ebiten.Image, entities []*ecs.Entity, width, height int) *ebiten.Image {
	dx, dy := dungeon.Size()
	camera := dungeon.SubImage(image.Rect(0, 0, width, height)).(*ebiten.Image)
	for _, entity := range ecs.FilterComponent(entities, &components.Focus{}, &components.Position{}) {
		position := entity.GetComponent(&components.Position{}).(*components.Position)
		cx := int(position.X * tileSize)
		cy := int(position.Y * tileSize)
		centerRect := toolbox.RectangleFromCenter(image.Point{cx, cy}, width, height)
		cameraRect := toolbox.RectangleWithBoundaries(centerRect, dx, dy)
		camera = dungeon.SubImage(cameraRect).(*ebiten.Image)
	}

	return camera
}

func (system *RenderSystem) drawDeck(entities []*ecs.Entity, x, y, width, height int) {
	screen := system.game.screen
	drawWindow(screen, x, y, width, height)

	for _, player := range ecs.FilterComponent(entities, &components.Control{}) {
		// If available for a suggestion then inform the player
		inRoom := player.GetComponent(&components.InRoom{})
		if inRoom != nil {
			room := (inRoom.(*components.InRoom)).Room
			str := fmt.Sprintf("[space] Make a suggestion in %s, or [x] an accusation", room)
			drawTextWithShadowCenter(screen, str, 0, y+fontSize, 1, fontColor, width)
		}
	}

	suspects := "Suspects\n--------------------\n"
	weapons := "Weapons\n--------------------\n"
	rooms := "Rooms\n--------------------\n"

	for _, p := range ecs.FilterComponent(entities, &components.Control{}, &components.Memory{}) {
		memory := p.GetComponent(&components.Memory{}).(*components.Memory)
		keys := []string{}
		for key := range memory.Suspects {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			pointer := ""
			selector := ""
			suspects += fmt.Sprintf("%s%s%-18s %s\n", pointer, selector, key, knownString(memory.Suspects[key]))
		}

		keys = []string{}
		for key := range memory.Weapons {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			pointer := ""
			selector := ""
			weapons += fmt.Sprintf("%s%s%-18s %s\n", pointer, selector, key, knownString(memory.Weapons[key]))
		}

		keys = []string{}
		for key := range memory.Rooms {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			pointer := ""
			selector := ""
			rooms += fmt.Sprintf("%s%s%-18s %s\n", pointer, selector, key, knownString(memory.Rooms[key]))
		}
	}

	colwidth := width / 5
	drawTextWithShadowCenter(screen, suspects, colwidth+fontSize, y+(3*fontSize), 1, fontColor, 256)
	drawTextWithShadowCenter(screen, weapons, (2*colwidth)+fontSize, y+(3*fontSize), 1, fontColor, 256)
	drawTextWithShadowCenter(screen, rooms, (3*colwidth)+fontSize, y+(3*fontSize), 1, fontColor, 256)
}

func (system *RenderSystem) drawPicker(entities []*ecs.Entity, x, y, width, height int, final bool) {
	title := "Make a suggestion"
	if final {
		title = "Make an accusation"
	}

	if !final {
		for _, e := range ecs.FilterComponent(entities, &components.Control{}, &components.InRoom{}) {
			inRoom := e.GetComponent(&components.InRoom{}).(*components.InRoom)
			selectedRoom = inRoom.Room
		}
	}

	str := ""
	for _, p := range ecs.FilterComponent(entities, &components.Control{}, &components.Memory{}) {
		memory := p.GetComponent(&components.Memory{}).(*components.Memory)
		index := 0

		if selectedIndex < 0 {
			selectedIndex = 0
		}

		str = fmt.Sprintf("%s\n\n", title)
		str += "Suspects\n"
		str += "---------------------------------------------------------------\n"

		keys := []string{}
		for key := range memory.Suspects {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			pointer := "   "
			selector := " "
			if selectedIndex == index {
				pointer = "-> "
				if selectedAction == ActionToggle {
					selectedAction = ActionNone
					if selectedSuspect != key {
						selectedSuspect = key
					} else {
						selectedSuspect = ""
					}
				}
			}
			if key == selectedSuspect {
				selector = "*"
			}
			str += fmt.Sprintf("%s%s%-20s %s\n", pointer, selector, key, knownString(memory.Suspects[key]))
			index += 1
		}
		str += "\n"

		str += "Weapons\n"
		str += "---------------------------------------------------------------\n"
		keys = []string{}
		for key := range memory.Weapons {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			pointer := "   "
			selector := " "
			if selectedIndex == index {
				pointer = "-> "
				if selectedAction == ActionToggle {
					selectedAction = ActionNone
					if selectedWeapon != key {
						selectedWeapon = key
					} else {
						selectedWeapon = ""
					}
				}
			}
			if key == selectedWeapon {
				selector = "*"
			}
			str += fmt.Sprintf("%s%s%-20s %s\n", pointer, selector, key, knownString(memory.Weapons[key]))
			index += 1
		}
		str += "\n"

		str += "Rooms\n"
		str += "---------------------------------------------------------------\n"
		keys = []string{}
		for key := range memory.Rooms {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		if !final && selectedIndex > index-1 {
			selectedIndex = index - 1
		}

		for _, key := range keys {
			pointer := "   "
			selector := " "
			if selectedIndex == index {
				pointer = "-> "
				if selectedAction == ActionToggle {
					selectedAction = ActionNone
					if selectedRoom != key {
						selectedRoom = key
					} else {
						selectedRoom = ""
					}
				}
			}
			if key == selectedRoom {
				selector = "*"
			}
			str += fmt.Sprintf("%s%s%-20s %s\n", pointer, selector, key, knownString(memory.Rooms[key]))
			index += 1
		}
		str += "\n"

		if final && selectedIndex > index {
			selectedIndex = index
		}
	}

	str += "\n[space] toggle selection [enter] commit [q] cancel"

	screen := system.game.screen
	drawWindow(screen, x, y, width, height)
	drawTextWithShadow(screen, str, x, y, 1, fontColor)
}

func knownString(value int) string {
	switch value {
	case 1:
		return "X"
	case 2:
		return "@"
	case 3:
		return "*"
	default:
		return " "
	}
}

func (system *RenderSystem) drawJournal(entities []*ecs.Entity, x, y, width, height int) {
	str := ""
	for _, p := range ecs.FilterComponent(entities, &components.Control{}, &components.Memory{}) {
		memory := p.GetComponent(&components.Memory{}).(*components.Memory)

		str = ""
		str += "Suspects\n"
		str += "---------------------------------------------------------------\n"

		keys := []string{}
		for key := range memory.Suspects {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			str += fmt.Sprintf("%-20s %s\n", key, knownString(memory.Suspects[key]))
		}
		str += "\n"

		str += "Weapons\n"
		str += "---------------------------------------------------------------\n"
		keys = []string{}
		for key := range memory.Weapons {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			str += fmt.Sprintf("%-20s %s\n", key, knownString(memory.Weapons[key]))
		}
		str += "\n"

		str += "Rooms\n"
		str += "---------------------------------------------------------------\n"
		keys = []string{}
		for key := range memory.Rooms {
			keys = append(keys, key)
		}
		sort.Strings(keys)

		for _, key := range keys {
			str += fmt.Sprintf("%-20s %s\n", key, knownString(memory.Rooms[key]))
		}
		str += "\n"
	}

	screen := system.game.screen
	drawWindow(screen, x, y, width, height)
	drawTextWithShadow(screen, str, x+fontSize, y+fontSize, 1, fontColor)
}

func Dedup(slice []string) []string {
	keys := make(map[string]bool)
	list := []string{}

	// This handles the indentifier present on each message.
	rx, _ := regexp.Compile(`[a-zA-Z0-9]+\s\([a-zA-Z0-9@]+\)\:`)

	// If the key(values of the slice) is not equal
	// to the already present value in new slice (list)
	// then we append it. else we jump on another element.
	for _, entry := range slice {
		normalized := rx.ReplaceAllString(entry, "")
		if _, value := keys[normalized]; !value {
			keys[normalized] = true
			list = append(list, entry)
		}
	}
	return list
}

func (system *RenderSystem) drawConsole(buffer *bytes.Buffer, x, y, width, height int) {
	max := 9
	lines := []string{}
	reader := bufio.NewReader(bytes.NewReader(buffer.Bytes()))
	for {
		line, _, err := reader.ReadLine()
		if err == io.EOF {
			break
		}
		lines = append(lines, string(line))
	}

	lines = Dedup(lines)

	var toDisplay []string
	if len(lines) > max {
		toDisplay = lines[len(lines)-max:]
	} else {
		toDisplay = lines
	}

	padding := fontSize
	screen := system.game.screen
	drawWindow(screen, x, y, width, height)
	buffer = bytes.NewBufferString(strings.Join(toDisplay, "\n"))
	drawTextWithShadow(screen, strings.Join(toDisplay, "\n"), x+padding, y+padding, 1, fontColor)

	commands := "    [W] or [Up]  Move north\n"
	commands += "  [A] or [Left]  Move west\n"
	commands += "  [S] or [Down]  Move south\n"
	commands += " [D] or [Right]  Move east\n"
	commands += "            [X]  Make an accusation\n"
	commands += "          [Tab]  View detective notes\n"
	commands += "[Q] or [Escape]  Quit game or current menu\n"
	drawTextWithShadow(screen, commands, (width-(21*fontSize))-padding, y+padding, 1, fontColor)
}

func drawCard(img *ebiten.Image, entity *ecs.Entity, x, y, width, height int) {
	borderwidth := 5
	ebitenutil.DrawRect(img, float64(x), float64(y), float64(width), float64(height), color.RGBA{0xff, 0xff, 0xff, 0xc0})
	ebitenutil.DrawRect(img, float64(x+borderwidth), float64(y+borderwidth), float64(width-(2*borderwidth)), float64(height-(2*borderwidth)), color.RGBA{0, 0, 0, 0xc0})

	description := entity.GetComponent(&components.Description{}).(*components.Description)
	drawTextWithShadow(img, description.Name, x+(2*borderwidth), y+(2*borderwidth), 1, color.RGBA{0xff, 0xff, 0xff, 0xc0})
}

func drawWindow(img *ebiten.Image, x, y, width, height int) {
	ebitenutil.DrawRect(img, float64(x), float64(y), float64(width), float64(height), color.RGBA{8, 8, 8, 0xc0})
}

func drawImageAt(screen, img *ebiten.Image, x, y, width, height int) {
	options := &ebiten.DrawImageOptions{}
	options.GeoM.Translate(float64(x), float64(y))
	screen.DrawImage(img, options)
}

func drawTextWithShadow(img *ebiten.Image, str string, x, y, scale int, clr color.Color) {
	offsetY := fontSize * scale
	for _, line := range strings.Split(str, "\n") {
		y += offsetY
		text.Draw(img, line, arcadeFont, x+1, y+1, color.NRGBA{0, 0, 0, 0x80})
		text.Draw(img, line, arcadeFont, x, y, clr)
	}
}

func drawTextWithShadowCenter(img *ebiten.Image, str string, x, y, scale int, clr color.Color, width int) {
	w := textWidth(str) * scale
	x += (width - w) / 2
	drawTextWithShadow(img, str, x, y, scale, clr)
}

func textWidth(str string) int {
	maxW := 0
	for _, line := range strings.Split(str, "\n") {
		b, _ := font.BoundString(arcadeFont, line)
		w := (b.Max.X - b.Min.X).Ceil()
		if maxW < w {
			maxW = w
		}
	}
	return maxW
}
