package gen

import (
	"encoding/json"
	"math/rand"

	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/toolbox"
)

// Recipe is a manifest of items to use during generation.
type Recipe struct {
	Items []RecipeItem `json:"items"`
}

// RecipeItem represents an entity made of components.
type RecipeItem struct {
	Components []RecipeComponent `json:"components"`
}

// RecipeProperty is a component property with random or set values.
type RecipeProperty struct {
	PropertyName string      `json:"name,omitempty"`
	Maximum      int32       `json:"maximum,omitempty"`
	Minimum      int32       `json:"minimum,omitempty"`
	Chance       int32       `json:"chance,omitempty"`
	Value        interface{} `json:"value,omitempty"`
}

// RecipeComponent refers to a component's type and a property set.
type RecipeComponent struct {
	Type       string           `json:"type"`
	Properties []RecipeProperty `json:"properties"`
}

// GenerateEntity randomizes the creation proccess of an entity provided a recipe from a JSON manifest.
func GenerateEntity(random *rand.Rand, recipe RecipeItem) *ecs.Entity {
	entity := ecs.NewEntity()

	// Loop through and assign components
	for _, c := range recipe.Components {
		// Get the component by type name
		component := transform[c.Type]()

		// Configure each property
		props := make(map[string]interface{})
		for _, p := range c.Properties {
			if p.PropertyName == "" {
				continue
			}

			if p.Maximum > 0 || p.Minimum > 0 {
				if p.Maximum == p.Minimum {
					props[p.PropertyName] = p.Minimum
				} else {
					props[p.PropertyName] = toolbox.BetweenInt32(random, p.Maximum, p.Minimum)
				}
			} else if p.Chance > 0 {
				if toolbox.BetweenInt(random, 100, 0) >= 50 {
					props[p.PropertyName] = true
				} else {
					props[p.PropertyName] = false
				}
			} else {
				props[p.PropertyName] = p.Value
			}
		}

		if bytes, err := json.Marshal(props); err == nil {
			component.FromJson(bytes)
			entity.AddComponent(component)
		}
	}

	return entity
}

// Component interface describes a component which may be
// associated with an entity. These are purely just data used
// to describe the state.
type JsonComponent interface {
	Type() string
	FromJson(buffer []byte) error
}

// transform a `RecipeItem` in to a component by their type value.
var transform = map[string]func() JsonComponent{
	"Bag":         func() JsonComponent { return &components.Bag{} },
	"Card":        func() JsonComponent { return &components.Card{} },
	"Control":     func() JsonComponent { return &components.Control{} },
	"Depth":       func() JsonComponent { return &components.Depth{} },
	"Description": func() JsonComponent { return &components.Description{} },
	"Drawable":    func() JsonComponent { return &components.Drawable{} },
	"Focus":       func() JsonComponent { return &components.Focus{} },
	"Identified":  func() JsonComponent { return &components.Identified{} },
	"InRoom":      func() JsonComponent { return &components.InRoom{} },
	"Item":        func() JsonComponent { return &components.Item{} },
	"Memory":      func() JsonComponent { return &components.Memory{} },
	"Motion":      func() JsonComponent { return &components.Motion{} },
	"Position":    func() JsonComponent { return &components.Position{} },
	"Solution":    func() JsonComponent { return &components.Solution{} },
	"Suggestion":  func() JsonComponent { return &components.Suggestion{} },
	"Trash":       func() JsonComponent { return &components.Trash{} },
	"Turn":        func() JsonComponent { return &components.Turn{} },
	"Turnable":    func() JsonComponent { return &components.Turnable{} },
	"Vision":      func() JsonComponent { return &components.Vision{} },
}
