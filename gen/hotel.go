package gen

import (
	"math/rand"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/dungeon"
)

func ConstructHotel(random *rand.Rand, width, height, levels int) []*cluedorl.Floor {
	// log.Println("constructing hotel")
	floors := []*cluedorl.Floor{}
	for i := 0; i < levels; i++ {
		gen := dungeon.NewHotelMapGenerator(width, height, 10+i, 4+i, random)
		m := gen.CreateMap()
		floor := &cluedorl.Floor{
			Depth: int32(i),
			Map:   m,
			Rooms: gen.Rooms[1:],
		}
		floors = append(floors, floor)
	}
	return floors
}
