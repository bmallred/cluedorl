package gen

import (
	"encoding/json"
	"log"
	"math/rand"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/assets"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/dungeon"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/toolbox"
)

// GetEmptyTile randomly from the dungeon map.
func GetEmptyTile(random *rand.Rand, level *dungeon.Map) (int, int) {
	for {
		x := toolbox.BetweenInt(random, level.Width-1, 0)
		y := toolbox.BetweenInt(random, level.Height-1, 0)
		if level.GetTile(x, y).Type == dungeon.TileEmpty {
			return x, y
		}
	}
}

// SpawnEntities randomly from an asset manifest.
func SpawnEntities(random *rand.Rand, asset string, minimum, maximum int, floors []*cluedorl.Floor, currentDepth int32, manager ecs.EntityManager) []*ecs.Entity {
	// log.Println("spawning entities")
	entities := []*ecs.Entity{}
	max := toolbox.BetweenInt(random, maximum, minimum)
	items, err := assets.Read(asset, true)
	if err != nil {
		log.Printf("failed to read asset manifest: %v", err)
		return entities
	}

	var manifest Recipe
	if err := json.Unmarshal(items, &manifest); err != nil {
		log.Printf("failed to parse asset: %v", err)
		return entities
	}

	for i := 0; i < max; i++ {
		index := toolbox.BetweenInt(random, len(manifest.Items), 1)
		entity := GenerateEntity(random, manifest.Items[index-1])

		positionComponent := entity.GetComponent(&components.Position{})
		depthComponent := entity.GetComponent(&components.Depth{})
		if positionComponent != nil && depthComponent != nil {
			depth := depthComponent.(*components.Depth)
			x, y := GetEmptyTile(random, floors[depth.Current].Map)

			position := positionComponent.(*components.Position)
			position.X = int32(x)
			position.Y = int32(y)
		}

		entities = append(entities, entity)
		manager.Add(entity)
	}
	return entities
}

// spawn one entity per item in asset manifest.
func SpawnEntitiesOnce(random *rand.Rand, asset string, floors []*cluedorl.Floor, currentDepth int32, manager ecs.EntityManager) []*ecs.Entity {
	// log.Println("spawning entities at least once")
	entities := []*ecs.Entity{}
	items, err := assets.Read(asset, true)
	if err != nil {
		log.Printf("failed to read asset manifest: %v", err)
		return entities
	}

	var manifest Recipe
	if err := json.Unmarshal(items, &manifest); err != nil {
		log.Printf("failed to parse asset: %v", err)
		return entities
	}

	for _, item := range manifest.Items {
		entity := GenerateEntity(random, item)
		positionComponent := entity.GetComponent(&components.Position{})
		depthComponent := entity.GetComponent(&components.Depth{})
		if positionComponent != nil && depthComponent != nil {
			depth := depthComponent.(*components.Depth)
			x, y := GetEmptyTile(random, floors[depth.Current].Map)

			position := positionComponent.(*components.Position)
			position.X = int32(x)
			position.Y = int32(y)
		}

		entities = append(entities, entity)
		manager.Add(entity)
	}
	return entities
}
