package gen

import (
	"encoding/json"
	"log"
	"math/rand"
	"sort"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/assets"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/toolbox"
)

// spawn one entity per item in asset manifest.
func SpawnCardsByCategory(random *rand.Rand, asset, category string, floors []*cluedorl.Floor, total int, currentDepth int32, manager ecs.EntityManager) []*ecs.Entity {
	// log.Println("spawning entities at least once")
	entities := []*ecs.Entity{}
	items, err := assets.Read(asset, true)
	if err != nil {
		log.Printf("failed to read asset manifest: %v", err)
		return entities
	}

	var manifest Recipe
	if err := json.Unmarshal(items, &manifest); err != nil {
		log.Printf("failed to parse asset: %v", err)
		return entities
	}

	for _, item := range manifest.Items {
		entity := GenerateEntity(random, item)
		cardComponent := entity.GetComponent(&components.Card{})
		if cardComponent == nil {
			continue
		}

		card := cardComponent.(*components.Card)
		if card.Category != category {
			continue
		}

		entities = append(entities, entity)
		manager.Add(entity)

		if total != -1 && len(entities) >= total {
			break
		}
	}
	return entities
}

func AssignRooms(floor *cluedorl.Floor, locations []*ecs.Entity) {
	if len(floor.Rooms) != len(locations) {
		log.Printf("Number of rooms [%d] do not match the number of location cards [%d]!", len(floor.Rooms), len(locations))
	}

	for i, location := range locations {
		location.AddComponent(&components.Room{
			Rectangle: floor.Rooms[i],
		})
	}
}

func WipeMemories(entities []*ecs.Entity, suspects, weapons, locations []*ecs.Entity) {
	for _, e := range ecs.FilterComponent(entities, &components.Bag{}, &components.Memory{}, &components.Description{}) {
		// Initialize memory with known items in the game
		memory := e.GetComponent(&components.Memory{}).(*components.Memory)
		memory.Suspects = map[string]int{}
		memory.Weapons = map[string]int{}
		memory.Rooms = map[string]int{}

		for _, c := range suspects {
			desc := c.GetComponent(&components.Description{}).(*components.Description)
			memory.Suspects[desc.Name] = components.CardUnknown
		}
		for _, c := range weapons {
			desc := c.GetComponent(&components.Description{}).(*components.Description)
			memory.Weapons[desc.Name] = components.CardUnknown
		}
		for _, c := range locations {
			desc := c.GetComponent(&components.Description{}).(*components.Description)
			memory.Rooms[desc.Name] = components.CardUnknown
		}

		// Store what they know from their own hand
		bag := e.GetComponent(&components.Bag{}).(*components.Bag)
		for _, c := range ecs.FilterComponent(bag.Items, &components.Card{}, &components.Description{}) {
			desc := c.GetComponent(&components.Description{}).(*components.Description)
			if _, ok := memory.Suspects[desc.Name]; ok {
				memory.Suspects[desc.Name] = components.CardOwned
				continue
			}
			if _, ok := memory.Weapons[desc.Name]; ok {
				memory.Weapons[desc.Name] = components.CardOwned
				continue
			}
			if _, ok := memory.Rooms[desc.Name]; ok {
				memory.Rooms[desc.Name] = components.CardOwned
				continue
			}
		}
	}
}

func DealCards(random *rand.Rand, players, characters, cards []*ecs.Entity) {
	// log.Println("players", len(players))
	// log.Println("characters", len(characters))
	// log.Println("cards", len(cards))

	participants := players[:]
	participants = append(participants, characters...)
	// log.Println("participants", len(participants))

	// Shuffle
	// log.Println("shuffling")
	times := toolbox.BetweenInt(random, 10, 3)
	for i := 0; i < times; i++ {
		random.Shuffle(len(cards), func(i, j int) { cards[i], cards[j] = cards[j], cards[i] })
	}

	// Select one of each category of card to be the final solution
	// log.Println("selecting solution")
	scard := false
	wcard := false
	rcard := false
	for _, card := range cards {
		cardComponent := card.GetComponent(&components.Card{}).(*components.Card)
		switch cardComponent.Category {
		case "Suspect":
			if scard {
				continue
			}
			scard = true
			card.AddComponent(&components.Solution{})
		case "Weapon":
			if wcard {
				continue
			}
			wcard = true
			card.AddComponent(&components.Solution{})
		case "Room":
			if rcard {
				continue
			}
			rcard = true
			card.AddComponent(&components.Solution{})
		}

		if scard && wcard && rcard {
			break
		}
	}

	// Shuffle
	times = toolbox.BetweenInt(random, 10, 3)
	for i := 0; i < times; i++ {
		random.Shuffle(len(cards), func(i, j int) { cards[i], cards[j] = cards[j], cards[i] })
	}

	// Select which participant will go first
	sort.Slice(participants[:], func(i, j int) bool {
		return participants[i].ID() < participants[j].ID()
	})
	index := toolbox.BetweenInt(random, len(participants)-1, 0)

	// Deal cards one at a time to each participant until there are no more
	for _, card := range cards {
		// Skip solution cards
		solution := card.GetComponent(&components.Solution{})
		if solution != nil {
			continue
		}

		var bag *components.Bag
		for bag == nil {
			bagComponent := participants[index].GetComponent(&components.Bag{})
			if bagComponent == nil {
				index += 1
				if index >= len(participants) {
					index = 0
				}
				continue
			}
			bag = bagComponent.(*components.Bag)
		}

		// Identify any cards assigned to the player
		controlComponent := participants[index].GetComponent(&components.Control{})
		if controlComponent != nil {
			card.AddComponent(&components.Identified{})
		}

		// Add the card to the participants bag
		bag.Items = append(bag.Items, card)

		// Go to the next participant
		index += 1
		if index >= len(participants) {
			index = 0
		}
	}
}
