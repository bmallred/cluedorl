package gen

import (
	"github.com/norendren/go-fov/fov"
)

func ConstructFov() *fov.View {
	return fov.New()
}
