package cluedorl

import (
	"image"

	"gitlab.com/bmallred/dungeon"
)

type Floor struct {
	Depth int32
	Map   *dungeon.Map
	Rooms []image.Rectangle
}
