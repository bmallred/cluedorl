package systems

import (
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
)

type SuggestionSystem struct {
	Options *cluedorl.GameOptions
}

// Update when a player may posit a suggestion
func (system *SuggestionSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] suggestion")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	// Check to see if a controllable entity has a turn
	participants := ecs.FilterComponent(entities, &components.Memory{})
	for _, e := range ecs.FilterComponent(entities, &components.Suggestion{}, &components.Memory{}) {
		suggestion := e.GetComponent(&components.Suggestion{}).(*components.Suggestion)
		memory := e.GetComponent(&components.Memory{}).(*components.Memory)
		memory.LastRoom = suggestion.Room

		if suggestion.Final {
			solutions := ecs.FilterComponent(entities, &components.Solution{})
			correct := Accuse(system.Options.Log, e, solutions, suggestion.Suspect, suggestion.Weapon, suggestion.Room)
			if correct {
				system.Options.Mode = cluedorl.Win
			} else {
				system.Options.Mode = cluedorl.Lose
			}
		} else {
			passes := Suggest(system.Options.Log, e, participants, suggestion.Suspect, suggestion.Weapon, suggestion.Room)
			if passes {
				ks, kw, kr := known(memory.Suspects), known(memory.Weapons), known(memory.Rooms)
				if ks != "" && kw != "" && kr != "" {
					// If the suggestion passes we need to check to see how close our memory says
					solutions := ecs.FilterComponent(entities, &components.Solution{})
					correct := Accuse(system.Options.Log, e, solutions, suggestion.Suspect, suggestion.Weapon, suggestion.Room)
					if correct {
						system.Options.Mode = cluedorl.Win
					} else {
						system.Options.Mode = cluedorl.Lose
					}
				}
			}
		}

		e.RemoveComponent(&components.Suggestion{})
	}
}
