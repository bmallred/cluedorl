package systems

import (
	"sort"
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
)

// TurnSystem is used to only permit actions when an entity has a turn.
type TurnSystem struct {
	Options *cluedorl.GameOptions
}

// Update the who's turn it is.
func (system *TurnSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] turn")

	// Find the entity who currently is having their turn and remove it.
	done := false
	eid := uint64(0)
	for _, e := range ecs.FilterComponent(entities, &components.Turn{}) {
		turn := e.GetComponent(&components.Turn{}).(*components.Turn)
		if turn.Taken {
			done = true
			eid = e.ID()
			e.RemoveComponent(&components.Turn{})
		}
	}

	if !done {
		return
	}

	// Order the entities by their ID
	byComponents := ecs.FilterComponent(entities, &components.Turnable{}, &components.Position{}, &components.Depth{})
	eligible := ecs.FilterBy(byComponents, func(entity *ecs.Entity) bool {
		depth := entity.GetComponent(&components.Depth{}).(*components.Depth)
		return depth.Current == system.Options.Depth
	})
	sort.Slice(eligible[:], func(a, b int) bool {
		return eligible[a].ID() < eligible[b].ID()
	})

	// Select the next entity to have a turn
	max := len(eligible) - 1
	target := 0
	for index, next := range eligible {
		if next.ID() == eid {
			if index < max {
				target = index + 1
			} else {
				target = 0
			}
		}
	}

	if max >= target {
		eligible[target].AddComponent(&components.Turn{})
	}
}
