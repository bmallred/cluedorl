package systems

import (
	"image"
	"sort"
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/dungeon"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/hound/astar"
	"gitlab.com/bmallred/toolbox"
)

type AiRoomSystem struct {
	Options *cluedorl.GameOptions
}

// Update participants (not players) thought process.
func (system *AiRoomSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] ai")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	// Check to see if a controllable entity has a turn
	players := ecs.FilterComponent(entities, &components.Control{}, &components.Motion{})
	for _, player := range players {
		c := player.GetComponent(&components.Turn{})
		if c != nil {
			return
		}
	}

	// Get all cards and then group them by category
	cards := ecs.FilterComponent(entities, &components.Card{})
	locations := ecs.FilterBy(cards, func(entity *ecs.Entity) bool {
		cardComponent := entity.GetComponent(&components.Card{})
		if cardComponent != nil {
			card := cardComponent.(*components.Card)
			return card.Category == "Room"
		}
		return false
	})

	floor := system.Options.CurrentLevel()
	graph := createGraph(system.Options.CurrentLevel().Map)
	participants := ecs.FilterComponent(entities, &components.Memory{}, &components.Motion{}, &components.Position{})

	costFunc := func(a, b image.Point) float64 {
		tile := floor.Map.GetTile(b.X, b.Y)

		// Blocked
		if tile.Type == dungeon.TileBlock {
			return 9999
		}

		// Check if another participant is currently on that square
		for _, e := range participants {
			position := e.GetComponent(&components.Position{}).(*components.Position)
			p := position.Point()
			if p.X == b.X && p.Y == b.Y {
				return 9999
			}
		}

		// Otherwise return the best route
		return toolbox.EuclideanDistanceFloat64(float64(a.X), float64(a.Y), float64(b.X), float64(b.Y))
	}

	// Go through each participant (not players)
	for _, e := range participants {
		// If the participant is controllable then skip
		control := e.GetComponent(&components.Control{})
		if control != nil {
			continue
		}

		// If the participant is not heading to a location location then figure out the next one
		position := e.GetComponent(&components.Position{}).(*components.Position)
		source := position.Point()
		target := NextRoom(locations, e)

		// If the participant is heading to a location then move that direction
		path := astar.FindPath(graph, source, target, astar.NeighborsCardinalFour, costFunc, costFunc)

		next := source
		if len(path) > 1 {
			next = path[1]
		}

		motion := e.GetComponent(&components.Motion{}).(*components.Motion)
		if next.X < source.X {
			motion.VelocityX -= 1
		} else if next.X > source.X {
			motion.VelocityX += 1
		} else if next.Y < source.Y {
			motion.VelocityY -= 1
		} else if next.Y > source.Y {
			motion.VelocityY += 1
		}
	}

	// Move the turn to the next player
	sort.Slice(players[:], func(a, b int) bool {
		return players[a].ID() < players[b].ID()
	})
	if len(players) > 0 {
		players[0].AddComponent(&components.Turn{})
	}
}

func createGraph(level *dungeon.Map) [][]int {
	graph := [][]int{}
	for x := 0; x < level.Width; x++ {
		row := []int{}
		for y := 0; y < level.Height; y++ {
			row = append(row, y)
		}
		graph = append(graph, row)
	}
	return graph
}

func NextRoom(rooms []*ecs.Entity, participant *ecs.Entity) image.Point {
	position := participant.GetComponent(&components.Position{}).(*components.Position)
	current := position.Point()

	// Continue to go to the next waypoint
	memory := participant.GetComponent(&components.Memory{}).(*components.Memory)
	waypointMatched := memory.NextWaypoint.X == memory.LastWaypoint.X && memory.NextWaypoint.Y == memory.LastWaypoint.Y
	waypointReached := memory.NextWaypoint.X == current.X && memory.NextWaypoint.Y == current.Y
	if !waypointMatched && !waypointReached {
		return memory.NextWaypoint
	}

	// Find the next waypoint
	waypoint := findKnownRoom(current, memory, rooms)
	if waypoint.X == -1 && waypoint.Y == -1 {
		waypoint = findUnknownRoom(current, memory, rooms)
	}
	if waypoint.X == -1 && waypoint.Y == -1 {
		waypoint = findNearestRoom(current, rooms)
	}

	// Update the memory
	memory.LastWaypoint, memory.NextWaypoint = memory.NextWaypoint, waypoint
	return waypoint
}

func findKnownRoom(position image.Point, memory *components.Memory, rooms []*ecs.Entity) image.Point {
	distances := map[int]image.Point{}

	for _, r := range ecs.FilterComponent(rooms, &components.Room{}, &components.Description{}) {
		room := r.GetComponent(&components.Room{}).(*components.Room)
		rr := room.Rectangle
		if position.X <= rr.Max.X-1 && position.X >= rr.Min.X+1 && position.Y <= rr.Max.Y-1 && position.Y >= rr.Min.Y+1 {
			// Currently in this room, let us choose another.
			continue
		}

		// Try to go to room the participant knows is the solution
		rc := toolbox.RectangleCenter(rr)
		rd := r.GetComponent(&components.Description{}).(*components.Description)
		for key, value := range memory.Rooms {
			if rd.Name == key && value == components.CardKnown {
				distance := toolbox.EuclideanDistanceInt(position.X, position.Y, rc.X, rc.Y)
				if _, ok := distances[distance]; !ok {
					distances[distance] = rc
				}
			}
		}
	}

	if len(distances) == 0 {
		return image.Pt(-1, -1)
	}

	keys := []int{}
	for k := range distances {
		keys = append(keys, k)
	}
	sort.Ints(keys)

	return distances[keys[0]]
}

func findUnknownRoom(position image.Point, memory *components.Memory, rooms []*ecs.Entity) image.Point {
	distances := map[int]image.Point{}
	for _, r := range ecs.FilterComponent(rooms, &components.Room{}, &components.Description{}) {
		room := r.GetComponent(&components.Room{}).(*components.Room)
		rr := room.Rectangle
		if position.X <= rr.Max.X-1 && position.X >= rr.Min.X+1 && position.Y <= rr.Max.Y-1 && position.Y >= rr.Min.Y+1 {
			// Currently in this room, let us choose another.
			continue
		}

		// Try to pick an unknown room
		rc := toolbox.RectangleCenter(rr)
		rd := r.GetComponent(&components.Description{}).(*components.Description)
		for key, value := range memory.Rooms {
			if rd.Name == key && value == components.CardUnknown {
				// Record the distances
				distance := toolbox.EuclideanDistanceInt(position.X, position.Y, rc.X, rc.Y)
				if _, ok := distances[distance]; !ok {
					distances[distance] = rc
				}
			}
		}
	}

	if len(distances) == 0 {
		return image.Pt(-1, -1)
	}

	keys := []int{}
	for k := range distances {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	return distances[keys[0]]
}

func findNearestRoom(position image.Point, rooms []*ecs.Entity) image.Point {
	distances := map[int]image.Point{}
	for _, r := range ecs.FilterComponent(rooms, &components.Room{}, &components.Description{}) {
		room := r.GetComponent(&components.Room{}).(*components.Room)
		rr := room.Rectangle
		if position.X <= rr.Max.X-1 && position.X >= rr.Min.X+1 && position.Y <= rr.Max.Y-1 && position.Y >= rr.Min.Y+1 {
			// Currently in this room, let us choose another.
			continue
		}

		// Record the distances
		rc := toolbox.RectangleCenter(rr)
		distance := toolbox.EuclideanDistanceInt(position.X, position.Y, rc.X, rc.Y)
		if _, ok := distances[distance]; !ok {
			distances[distance] = rc
		}
	}

	keys := []int{}
	for k := range distances {
		keys = append(keys, k)
	}
	sort.Ints(keys)
	return distances[keys[0]]
}
