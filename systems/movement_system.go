package systems

import (
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
)

// MovementSystem is used to provide motion to entities.
type MovementSystem struct {
	Options *cluedorl.GameOptions
}

// Update the location of entities with the motion
func (system *MovementSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] movement")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	for _, e := range ecs.FilterComponent(entities, &components.Motion{}, &components.Position{}) {
		position := e.GetComponent(&components.Position{}).(*components.Position)
		motion := e.GetComponent(&components.Motion{}).(*components.Motion)

		if int(motion.VelocityX) != 0 {
			position.X += int32(motion.VelocityX)
			endTurn(e)
		}

		if int(motion.VelocityY) != 0 {
			position.Y += int32(motion.VelocityY)
			endTurn(e)
		}

		if motion.Rest {
			motion.Rest = false
			endTurn(e)
		}
	}
}

// endTurn of the current entity if available.
func endTurn(entity *ecs.Entity) {
	t := entity.GetComponent(&components.Turn{})
	if t != nil {
		turn := t.(*components.Turn)
		turn.Taken = true
	}
}
