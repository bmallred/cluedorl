package systems

import (
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
)

// FovSystem detects collisions with non-item entities.
type FovSystem struct {
	Options *cluedorl.GameOptions
}

// Update the field of view map(s).
func (system *FovSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] fov")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	level := system.Options.CurrentLevel()
	fovMap := system.Options.Fov
	for _, player := range ecs.FilterComponent(entities, &components.Control{}, &components.Position{}, &components.Vision{}) {
		playerPosition := player.GetComponent(&components.Position{}).(*components.Position)
		playerVision := player.GetComponent(&components.Vision{}).(*components.Vision)
		fovMap.Compute(level.Map, playerPosition.Point().X, playerPosition.Point().Y, playerVision.Radius)
	}
}
