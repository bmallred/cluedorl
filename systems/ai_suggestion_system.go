package systems

import (
	"fmt"
	"log"
	"math/rand"
	"sort"
	"strings"
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/toolbox"
)

type AiSuggestionSystem struct {
	Options *cluedorl.GameOptions
}

// Update suggestions by participants
func (system *AiSuggestionSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] suggestion")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	// Check to see if a controllable entity has a turn
	players := ecs.FilterComponent(entities, &components.Control{}, &components.Motion{})
	for _, player := range players {
		c := player.GetComponent(&components.Turn{})
		if c != nil {
			return
		}
	}

	// Get all cards and then group them by category
	cards := ecs.FilterComponent(entities, &components.Card{})
	locations := ecs.FilterBy(cards, func(entity *ecs.Entity) bool {
		card := entity.GetComponent(&components.Card{}).(*components.Card)
		return card.Category == "Room"
	})

	participants := ecs.FilterComponent(entities, &components.Memory{})

	// Go through each participant (not players)
	for _, e := range participants {
		// If the participant is controllable then skip
		control := e.GetComponent(&components.Control{})
		if control != nil {
			continue
		}

		room := ""

		// Only allow a suggestion if they have reached their waypoint (destination)
		memory := e.GetComponent(&components.Memory{}).(*components.Memory)
		position := e.GetComponent(&components.Position{}).(*components.Position)
		current := position.Point()
		for _, r := range ecs.FilterComponent(locations, &components.Room{}, &components.Description{}) {
			rm := r.GetComponent(&components.Room{}).(*components.Room)
			rc := toolbox.RectangleCenter(rm.Rectangle)
			if current.X != rc.X && current.Y != rc.Y {
				continue
			}

			description := r.GetComponent(&components.Description{}).(*components.Description)
			room = description.Name
			break
		}

		// If not available for a suggestion then continue to next participant
		if room == "" {
			memory.LastRoom = room
			continue
		}

		// Check their memory to make a deduction
		if memory.LastRoom == room {
			continue
		}
		memory.LastRoom = room
		suspect := deduce(memory.Suspects)
		weapon := deduce(memory.Weapons)

		// Record response
		passes := Suggest(system.Options.Log, e, participants, suspect, weapon, room)
		if passes {
			// If the suggestion passes we need to check to see how close our memory says
			ks, kw, kr := known(memory.Suspects), known(memory.Weapons), known(memory.Rooms)

			// Try some luck
			luck := toolbox.BetweenInt(system.Options.Random, 50, 0)
			confidence := -1
			if ks != "" {
				confidence += 25
			}
			if kw != "" {
				confidence += 25
			}
			if kr != "" {
				confidence += 25
			}

			if confidence+luck >= 100 || (ks != "" && kw != "" && kr != "") {
				as, aw, ar := ks, kw, kr
				if as == "" {
					as = suspect
				}
				if aw == "" {
					aw = weapon
				}
				if ar == "" {
					ar = room
				}

				solutions := ecs.FilterComponent(entities, &components.Solution{})
				correct := Accuse(system.Options.Log, e, solutions, as, aw, ar)
				if correct {
					system.Options.Mode = cluedorl.Lose
				}
			}
		}
	}

	// system.Options.Log.Println("[Narrator] Psst... your turn.")
}

func deduce(memory map[string]int) string {
	// Check if any are known as the answer
	for key, value := range memory {
		if value == components.CardKnown {
			return key
		}
	}

	// Check if any are unknown
	for key, value := range memory {
		if value == components.CardUnknown {
			return key
		}
	}

	// Just select something
	for key, value := range memory {
		if value == components.CardOwned {
			return key
		}
	}

	return ""
}

func known(memory map[string]int) string {
	for key, value := range memory {
		if value == components.CardKnown {
			return key
		}
	}
	return ""
}

func mark(memory map[string]int, name string) {
	if name == "" {
		return
	}

	// If the card is already known then do not tamper with memory
	for _, value := range memory {
		if value == components.CardKnown {
			return
		}
	}

	for key, value := range memory {
		if key == name && value == components.CardUnknown {
			memory[key] = components.CardDiscarded
		}
	}

	// Check to see if we can deduce what the card is
	count := 0
	for _, value := range memory {
		if value == components.CardUnknown {
			count += 1
		}
	}

	if count == 1 {
		for key, value := range memory {
			if value == components.CardUnknown {
				memory[key] = components.CardKnown
			}
		}
	}
}

func scoresheet(participant *ecs.Entity, memory *components.Memory) {
	desc := participant.GetComponent(&components.Description{}).(*components.Description)

	str := fmt.Sprintf("\n%s\n\n", desc.Name)
	str += "Suspects\n"
	str += "---------------------------------------------------------------\n"

	keys := []string{}
	for key := range memory.Suspects {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		str += fmt.Sprintf("%s %d\n", key, memory.Suspects[key])
	}
	str += "\n"

	str += "Weapons\n"
	str += "---------------------------------------------------------------\n"
	keys = []string{}
	for key := range memory.Weapons {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		str += fmt.Sprintf("%s %d\n", key, memory.Weapons[key])
	}
	str += "\n"

	str += "Rooms\n"
	str += "---------------------------------------------------------------\n"
	keys = []string{}
	for key := range memory.Rooms {
		keys = append(keys, key)
	}
	sort.Strings(keys)

	for _, key := range keys {
		str += fmt.Sprintf("%s %d\n", key, memory.Rooms[key])
	}
	str += "\n"

	log.Println(str)
}

func Observe(logger *log.Logger, observer, refuter *ecs.Entity, suspect, weapon, room string) {
	// memory := observer.GetComponent(&components.Memory{}).(*components.Memory)
	// mark(memory.Suspects, suspect)
	// mark(memory.Weapons, weapon)
	// mark(memory.Rooms, room)
}

func RespondTo(logger *log.Logger, asker, refuter *ecs.Entity, suspect, weapon, room string) (string, string, string) {
	responses := [][]string{}
	bag := refuter.GetComponent(&components.Bag{}).(*components.Bag)
	for _, item := range ecs.FilterComponent(bag.Items, &components.Card{}, &components.Description{}) {
		desc := item.GetComponent(&components.Description{}).(*components.Description)
		if desc.Name == "" {
			continue
		}

		if suspect == desc.Name {
			responses = append(responses, []string{suspect, "", ""})
		}

		if weapon == desc.Name {
			responses = append(responses, []string{"", weapon, ""})
		}

		if room == desc.Name {
			responses = append(responses, []string{"", "", room})
		}
	}

	if len(responses) == 0 {
		return "", "", ""
	}

	r := rand.New(rand.NewSource(time.Now().Unix()))
	r.Shuffle(len(responses), func(i, j int) { responses[i], responses[j] = responses[j], responses[i] })
	return responses[0][0], responses[0][1], responses[0][2]
}

func See(logger *log.Logger, asker, refuter *ecs.Entity, suspect, weapon, room string) {
	if suspect == "" && weapon == "" && room == "" {
		return
	}

	ad := asker.GetComponent(&components.Description{}).(*components.Description)
	rd := refuter.GetComponent(&components.Description{}).(*components.Description)
	logger.Printf("%s shows %s %s", rd.Name, ad.Name, strings.TrimSpace(suspect+room+weapon))

	memory := asker.GetComponent(&components.Memory{}).(*components.Memory)
	mark(memory.Suspects, suspect)
	mark(memory.Weapons, weapon)
	mark(memory.Rooms, room)
}

func Suggest(logger *log.Logger, asker *ecs.Entity, participants []*ecs.Entity, suspect, weapon, room string) bool {
	description := asker.GetComponent(&components.Description{}).(*components.Description)
	me := description.Name
	logger.Printf("%s suggests %s in the %s with the %s", me, suspect, room, weapon)

	for _, refuter := range participants {
		if asker.ID() == refuter.ID() {
			continue
		}

		// Get the response
		rs, rw, rr := RespondTo(logger, asker, refuter, suspect, weapon, room)

		// Let everyone know what the results of the last response was
		for _, observer := range participants {
			if observer.ID() != refuter.ID() {
				Observe(logger, observer, refuter, suspect, weapon, room)
			}
		}

		See(logger, asker, refuter, rs, rw, rr)

		if rs == "" && rw == "" && rr == "" {
			rd := refuter.GetComponent(&components.Description{}).(*components.Description)
			logger.Printf("%s passes", rd.Name)
			continue
		}

		return false
	}

	return true
}

func Accuse(logger *log.Logger, asker *ecs.Entity, solutions []*ecs.Entity, suspect, weapon, room string) bool {
	description := asker.GetComponent(&components.Description{}).(*components.Description)
	me := description.Name
	logger.Printf("%s accuses %s in the %s with the %s!", me, suspect, room, weapon)

	correct := 0
	for _, item := range ecs.FilterComponent(solutions, &components.Card{}, &components.Description{}) {
		desc := item.GetComponent(&components.Description{}).(*components.Description)
		if suspect == desc.Name {
			correct += 1
		}
		if weapon == desc.Name {
			correct += 1
		}
		if room == desc.Name {
			correct += 1
		}
	}

	if correct == len(solutions) {
		logger.Printf("It appears %s was correct!", me)
		return true
	}

	asker.RemoveComponent(&components.Position{}, &components.Motion{})
	logger.Printf("%s accusation has been proven false and must pay for their libel!", me)
	return false
}
