package systems

import (
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/ecs"
)

type InRoomSystem struct {
	Options *cluedorl.GameOptions
}

// Update when a player is inside a room
func (system *InRoomSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] suggestion")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	// Check to see if a controllable entity has a turn
	players := ecs.FilterComponent(entities, &components.Control{}, &components.Memory{}, &components.Position{}, &components.Motion{})
	for _, player := range players {
		// Check only when it is my turn
		c := player.GetComponent(&components.Turn{})
		if c == nil {
			continue
		}

		// Clear state
		player.RemoveComponent(&components.InRoom{})

		// Get all cards and then group them by category
		cards := ecs.FilterComponent(entities, &components.Card{})
		locations := ecs.FilterBy(cards, func(entity *ecs.Entity) bool {
			card := entity.GetComponent(&components.Card{}).(*components.Card)
			return card.Category == "Room"
		})

		// Only allow a suggestion if they have reached their waypoint (destination)
		room := ""
		position := player.GetComponent(&components.Position{}).(*components.Position)
		current := position.Point()
		for _, r := range ecs.FilterComponent(locations, &components.Room{}, &components.Description{}) {
			rm := r.GetComponent(&components.Room{}).(*components.Room)
			rr := rm.Rectangle
			if current.X <= rr.Max.X-1 && current.X >= rr.Min.X+1 && current.Y <= rr.Max.Y-1 && current.Y >= rr.Min.Y+1 {
				description := r.GetComponent(&components.Description{}).(*components.Description)
				room = description.Name
				break
			}
		}

		// If not available for a suggestion then continue to next player
		memory := player.GetComponent(&components.Memory{}).(*components.Memory)
		if room == "" {
			continue
		}

		// Check their memory to make a deduction
		if memory.LastRoom == room {
			continue
		}

		player.AddComponent(&components.InRoom{
			Room: room,
		})
	}
}
