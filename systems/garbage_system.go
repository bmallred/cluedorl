package systems

import (
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/ecs"
)

// GarbageSystem will remove any entities with the trash
type GarbageSystem struct {
	EntityManager ecs.EntityManager
	Options       *cluedorl.GameOptions
}

// Update the game by removing entities marked for trash.
func (system *GarbageSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] garbage")
}
