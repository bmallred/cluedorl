package systems

import (
	"time"

	"gitlab.com/bmallred/cluedorl"
	"gitlab.com/bmallred/cluedorl/components"
	"gitlab.com/bmallred/dungeon"
	"gitlab.com/bmallred/ecs"
	"gitlab.com/bmallred/toolbox"
)

// CollisionSystem detects collisions with non-item entities.
type CollisionSystem struct {
	Options *cluedorl.GameOptions
}

// Update components when a collision has been detected with a non-item.
func (system *CollisionSystem) Update(delta time.Duration, entities []*ecs.Entity) {
	// log.Printf("[system] collision")

	if system.Options.Mode == cluedorl.Win || system.Options.Mode == cluedorl.Lose {
		return
	}

	// Collision with things
	for _, player := range ecs.FilterComponent(entities, &components.Control{}, &components.Motion{}, &components.Position{}, &components.Depth{}) {
		for _, thing := range ecs.FilterComponent(entities, &components.Position{}, &components.Depth{}) {
			if player.ID() == thing.ID() {
				continue
			}

			thingDepth := thing.GetComponent(&components.Depth{}).(*components.Depth)
			playerDepth := player.GetComponent(&components.Depth{}).(*components.Depth)
			if thingDepth.Current != playerDepth.Current {
				continue
			}

			thingPosition := thing.GetComponent(&components.Position{}).(*components.Position)
			playerPosition := player.GetComponent(&components.Position{}).(*components.Position)

			if toolbox.CollisionPoint(playerPosition.Point(), thingPosition.Point()) {
				// If the thing is an item then skip it
				item := thing.GetComponent(&components.Item{})
				if item != nil {
					continue
				}

				// Bounce the player back
				playerMotion := player.GetComponent(&components.Motion{}).(*components.Motion)
				bounce(playerPosition, playerMotion)

				// Bounce the thing back if it has motion
				thingMotion := thing.GetComponent(&components.Motion{}).(*components.Motion)
				if thingMotion != nil {
					bounce(thingPosition, thingMotion)
				}
			}
		}
	}

	// Check bounds on screens
	for _, e := range ecs.FilterComponent(entities, &components.Motion{}, &components.Position{}) {
		position := e.GetComponent(&components.Position{}).(*components.Position)
		motion := e.GetComponent(&components.Motion{}).(*components.Motion)

		// Collision with wall
		level := system.Options.CurrentLevel()
		if level.Map.GetTile(int(position.X), int(position.Y)).Type != dungeon.TileEmpty {
			bounce(position, motion)
		}

		motion.VelocityX = 0
		motion.VelocityY = 0
	}
}

// bounce the entity's position back after a collision.
func bounce(position *components.Position, motion *components.Motion) {
	if int(motion.VelocityX) != 0 {
		position.X -= int32(motion.VelocityX)
	}
	if int(motion.VelocityY) != 0 {
		position.Y -= int32(motion.VelocityY)
	}
}
